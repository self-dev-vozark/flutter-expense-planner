import '../models/transaction.dart';
import 'package:flutter/material.dart';
import './transaction_item.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deleteTransaction;

  TransactionList(this.transactions, this.deleteTransaction);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: transactions.isEmpty
            ? LayoutBuilder(builder: (ctx, constraints) {
                return Column(
                  children: <Widget>[
                    Text('No transactions added yet',
                        style: Theme.of(context).textTheme.headline6),
                    SizedBox(height: constraints.maxHeight * 0.05),
                    Container(
                        height: constraints.maxHeight * 0.6,
                        child: Image.asset(
                          'assets/images/img1.jpg',
                          fit: BoxFit.cover,
                        ))
                  ],
                );
              })
            : ListView(
                children: transactions.map((trx) {
                  return TransactionItem(
                      key: ValueKey(trx.id),
                      transaction: trx,
                      deleteTransaction: deleteTransaction);
                }).toList(),
              ));
  }
}
