import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class AdaptiveButton extends StatelessWidget {
  final String buttonText;
  final VoidCallback onPressedAction;

  AdaptiveButton(this.buttonText, this.onPressedAction);

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? CupertinoButton(
            child: Text(buttonText,
                style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold)),
            onPressed: onPressedAction,
          )
        : TextButton(
            child: Text(buttonText),
            onPressed: onPressedAction,
            style: TextButton.styleFrom(
                textStyle: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold)),
          );
  }
}
