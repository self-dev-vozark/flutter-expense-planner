import 'package:expense_planner/components/chart_bar.dart';
import 'package:expense_planner/models/transaction.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Chart extends StatelessWidget {
  final List<Transaction> recentTransactions;

  Chart(this.recentTransactions);

  List<Map<String, Object>> get groupedTransactionValues {
    return List.generate(7, (index) {
      final weekDay = DateTime.now().subtract(Duration(days: index));
      double totalSum = 0.0;

      for (var currentTrx in recentTransactions) {
        if (currentTrx.date.day == weekDay.day &&
            currentTrx.date.month == weekDay.month &&
            currentTrx.date.year == weekDay.year) {
          totalSum += currentTrx.amount;
        }
      }

      return {'day': DateFormat.E().format(weekDay), 'amount': totalSum};
    }).reversed.toList();
  }

  double get maxSpending {
    return groupedTransactionValues.fold(
        0.0, (sum, item) => sum += item['amount'] as double);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        elevation: 6,
        margin: EdgeInsets.all(20),
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: groupedTransactionValues.map((item) {
              return Flexible(
                fit: FlexFit.tight,
                child: ChartBar(
                    item['day'] as String,
                    item['amount'] as double,
                    maxSpending == 0
                        ? 0.0
                        : (item['amount'] as double) / maxSpending),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}
