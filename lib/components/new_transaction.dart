import 'dart:io';

import 'package:expense_planner/components/globals/adaptive_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NewTransaction extends StatefulWidget {
  final Function addTrx;

  NewTransaction(this.addTrx);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final _titleController = TextEditingController();
  final _amountController = TextEditingController();
  DateTime? _selectedDate;

  void _submitTrx() {
    if (_titleController.text.isEmpty) {
      return;
    }

    final submittedTitle = _titleController.text;
    final submittedAmount = double.parse(_amountController.text);

    if (submittedTitle.isEmpty ||
        submittedAmount <= 0 ||
        _selectedDate == null) {
      return;
    }
    widget.addTrx(_titleController.text, double.parse(_amountController.text),
        _selectedDate);

    Navigator.of(context).pop();
  }

  void _presentDatePicker() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2021),
            lastDate: DateTime.now())
        .then((value) {
      print(value);
      if (value == null) return;
      setState(() {
        _selectedDate = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Card(
          elevation: 5,
          child: Container(
            padding: EdgeInsets.only(
                top: 10,
                left: 10,
                right: 10,
                bottom: MediaQuery.of(context).viewInsets.bottom + 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                TextField(
                    decoration: InputDecoration(labelText: 'Title'),
                    controller: _titleController,
                    onSubmitted: (_) => _submitTrx()),
                TextField(
                    decoration: InputDecoration(labelText: 'Amount'),
                    controller: _amountController,
                    onSubmitted: (_) => _submitTrx(),
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true)),
                Container(
                  height: 70,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        _selectedDate == null
                            ? 'No date chosen'
                            : DateFormat.yMMMMd().format(_selectedDate!),
                      ),
                      AdaptiveButton('Choose Date', _presentDatePicker)
                    ],
                  ),
                ),
                Platform.isIOS
                    ? CupertinoButton(
                        child: Text('Add Transaction',
                            style: Theme.of(context).textTheme.button),
                        onPressed: _submitTrx,
                        color: Theme.of(context).primaryColor,
                      )
                    : ElevatedButton(
                        onPressed: _submitTrx,
                        child: Text('Add Transaction'),
                        style: ElevatedButton.styleFrom(
                            primary: Theme.of(context).primaryColor,
                            textStyle: TextStyle(
                                color:
                                    Theme.of(context).textTheme.button?.color)),
                      )
              ],
            ),
          )),
    );
  }
}
